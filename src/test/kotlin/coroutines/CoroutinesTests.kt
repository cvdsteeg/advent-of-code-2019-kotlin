package coroutines

import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.util.concurrent.atomic.AtomicInteger
import kotlin.system.measureTimeMillis

class CoroutinesTests {
    @Test
    fun givenHugeAmountOfCoroutines_whenStartIt_thenShouldExecuteItWithoutOutOfMemory() {
        runBlocking {
            // given
            val counter = AtomicInteger(0)
            val numberOfCoroutines = 1_000_000

            // when
            val jobs = List(numberOfCoroutines) {
                launch {
                    delay(1000L)
                    counter.incrementAndGet()
                }
            }
            jobs.forEach { it.join() }

            // then
            assertEquals(counter.get(), numberOfCoroutines)
        }
    }

    @Test
    fun fibonacci_test(){
        val fibonacciSeq = sequence {
            var a = 0
            var b = 1

            yield(1)

            while (true) {
                yield(a + b)

                val tmp = a + b
                a = b
                b = tmp
            }
        }

        val res = fibonacciSeq
                .take(12)
                .toList()

        assertEquals(res, listOf(1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144))
    }

    @Test
    fun givenAsyncCoroutine_whenStartIt_thenShouldExecuteItInTheAsyncWay() {
        // given
        val res = mutableListOf<String>()

        // when
        runBlocking {
            val job = launch {
                expensiveComputation(res)
            }
            res.add("Hello,")
            job.join()
        }

        // then
        assertEquals(res, listOf("Hello,", "word!"))
    }

    suspend fun expensiveComputation(res: MutableList<String>) {
        delay(1000L)
        res.add("word!")
    }

    @Test
    fun givenHaveTwoExpensiveAction_whenExecuteThemAsync_thenTheyShouldRunConcurrently() {
        runBlocking {
            val delay = 1000L
            val time = measureTimeMillis {
                // given
                val one = async {
                    someExpensiveComputation(delay)
                }
                val two = async {
                    someExpensiveComputation(delay)
                }

                // when
                runBlocking {
                    one.await()
                    two.await()
                }
            }

            // then
            assertTrue(time > delay && time < delay * 2)
        }
    }

    @Test
    fun givenTwoExpensiveAction_whenExecuteThemLazy_thenTheyShouldNotConcurrently() {
        runBlocking {
            val delay = 1000L
            val time = measureTimeMillis {
                // given
                val one
                        = async(start = CoroutineStart.LAZY) {
                    someExpensiveComputation(delay)
                }
                val two
                        = async(start = CoroutineStart.LAZY) {
                    someExpensiveComputation(delay)
                }

                // when
                runBlocking {
                    one.await()
                    two.await()
                }
            }

            // then
            assertTrue(time > delay * 2)
        }
    }


    private suspend fun someExpensiveComputation(delay: Long){
        delay(delay)
    }
}