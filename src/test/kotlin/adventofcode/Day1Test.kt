package adventofcode

import org.junit.jupiter.api.Assertions.assertEquals

import org.junit.jupiter.api.Test

internal class Day1Test {

    @Test
    fun assignment1() {
        assertEquals(439, Day1().assignment1())
    }

    @Test
    fun assignment2test1() {
        val ass2 = Day1(listOf(3, 3, 4, -2, -4)).assignment2()
        assertEquals(10, ass2)
    }

    @Test
    fun assignment2test2() {
        val ass2 = Day1(listOf(-6, +3, +8, +5, -6)).assignment2()
        assertEquals(5, ass2)
    }

    @Test
    fun assignment2() {
        val ass2 = Day1().assignment2()
        assertEquals(124645, ass2)
    }
}